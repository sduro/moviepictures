MoviePictures
-------------

Programa en pyhton para descargar las portadas de las peliculas.
Introduces el nombre de la pelicula y si es "único" directamente te descarga la foto por defecto.

DownScripts
-----------
Programa similar al anterior (MoviePictures) que sirve para descargar los scripts de las peliculas mostradas en la página http://www.dailyscript.com/movie.html

txt2pdf
-------
Programa en python que pasa de txt a pdf como el conocido `txt2pdf`
