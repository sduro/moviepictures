#!/usr/bin/python
from HTMLParser import HTMLParser
import urllib2, downScripts

from sgmllib import SGMLParser
import sys,urllib,math,os,time
import string

class URLLister(SGMLParser):
    def reset(self):                              
        SGMLParser.reset(self)
        self.urls = []

    def start_a(self, attrs):                    
        href = [v for k, v in attrs if k=='href']
        if href:
            self.urls.extend(href)

def download(picture):
	filename = picture.split('/')[-1]
	f = open(filename,'wb')
	response_pic = urllib2.Request(picture)
	handlepic = urllib2.urlopen(response_pic)
	meta = handlepic.info()
	filesize = int(meta.getheaders("Content-Length")[0])
	
	filesize_dl = 0
	block_sz = 8192
	while True:
		buffer = handlepic.read(block_sz)
		if not buffer:
			break
			
		filesize_dl += len(buffer)
		f.write(buffer)
		status = r"%10d [%3.2f%%]" % (filesize_dl,filesize_dl * 100. / filesize)
		status = status + chr(8)*(len(status)+1)
		print status
	f.close()
	
def main():
	CURDIR = os.getcwd()
	sys.path.append(CURDIR)
	#titol = raw_input('Movie name: ')
	url_base = 'http://www.dailyscript.com/movie.html'
	#stype = '&stype=title'
	#url = url_base + titol + stype
	url_download_base='http://www.dailyscript.com/'
	url=url_base
	
	print url
	response = urllib2.Request(url)
	handle = urllib2.urlopen(response)
	#print handle.read()
	parser = downScripts.URLLister()
	parser.feed(handle.read())
	#print parser.urls
	for links in parser.urls:
		if links.startswith('sc'):
			download(url_download_base+links)
	handle.close()

if __name__=='__main__':
	main()
